# CPSC 583 (Introduction to Information Visualization) at the University of Calgary D3.js Programming Assignment

### By: Elzanne Venter

## Purpose

<div>The purpose of this assignment was to create a 'brushing and linking' interaction for two visualizations provided by the instructor. The visuals to connect were a world map and a scatter plot. </div>

## Process

<div>For this assignment, I decided to follow the ‘Stream B: implementation’ option in the assignment description. To
do this, I first examined the code we were provided to understand how it works. To help in this
process, I added comments to the code. Then, I got a sense of the data and visuals by adding
functionality to the ‘mouseover’ event. This started with just printing to the console the country
name. Then I added a ‘click’ event instead and, again, printed the country name based on the
element in the map that was clicked. At this point, I felt confident to start doing the selection of
the elements. To show the viewer what they have selected by their button clicks, I decided to
outline the selection in a green stroke. I also decided that a second button click of an outlined
element would deselect the element. I believe this interaction is common in applications and,
thus, is a familiar interaction to potential viewers. To implement this, I created a toggle
functionality by using the following tertiary operators for the stroke color and the stroke width:
let colortoset = this.style.stroke === 'green' ? 'none' : 'green';
let sizetoset = this.style.strokeWidth === '2' ? '0.3' : '2';
To print the selection of data to the console, I stored the selected items in two different arrays,
one for the map and the other for the scatter plot. The map array is printed if the user selected
a country on the map and the dot array is printed if the user selected a dot in the scatter plot.
When the user selects an item on the map, if the corresponding dot exists, it is outlined as well
and vice versa.</div>

## Final Result

<div>The final interactive visualization is available [here](https://elzanne1.github.io/583A2/) </div> 
